<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Databases\Migrations\Migration;
use Illuminate\Databases\Schema\Blueprint;
use Illuminate\Suport\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;


class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $query = DB::table('posts')->insert([
            "title" => $request["title"],
            "body" => $request["'body"]
        ]);
        return redirect('/posts/create');
    }

    public function index()
    {
        return view('posts.index');
    }
}
