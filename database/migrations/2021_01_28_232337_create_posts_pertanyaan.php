<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_pertanyaan', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->longText('judul');
            $table->date('tanggal_dibuat');
            $table->integer('jawabang_tepat_id');
            $table->integer('profil_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_pertanyaan');
    }
}
